﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Interface
{
    [DataContract]
    public class Prescription : MedicalRecord
    {
        [DataMember]
        public bool isRealised { get; private set; }
        public Prescription(int diagnostic, string body, bool realised) : base(diagnostic, body)
        {
            this.isRealised = realised;
        }
    }
}
