﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    [Serializable]
    public class Diagnosis : MedicalRecord
    {
        public Diagnosis(int diagnostic, string body) : base(diagnostic, body)
        {

        }
    }
}
