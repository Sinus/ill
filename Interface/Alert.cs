﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    [Serializable]
    public class Alert
    {
        public int doctor { get; private set; }
        public string body { get; private set; }

        public Alert(int doctor, string body)
        {
            this.doctor = doctor;
            this.body = body;
        }
    }
}
