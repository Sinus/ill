﻿using System;
using System.ServiceModel;


namespace Interface
{
    public interface IClient
    {
        [OperationContract(IsOneWay = true)]
        void BroadcastMessage(string message);
    }
}
