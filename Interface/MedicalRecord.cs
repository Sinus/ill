﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    [DataContract]
    abstract public class MedicalRecord
    {
        [DataMember]
        protected int diagnostic;
        [DataMember]
        public string body { get; protected set; }

        public MedicalRecord(int diagnostic, string body)
        {
            this.diagnostic = diagnostic;
            this.body = body;
        }

        virtual public int GetDiagnostic()
        {
            return diagnostic;
        }
    }
}
