﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Interface
{
    [DataContract]
    public class Patient
    {
        [DataMember]
        public string surname { get; private set; }
        [DataMember]
        public string name { get; private set; }
        [DataMember]
        public string PESEL { get; private set; }
        
        [DataMember]
        public List<Treatment> treatmentHistory { get; private set; }
        [DataMember]
        public List<Diagnosis> diagnosisHistory { get; private set; }
        [DataMember]
        public List<Test> testHistory { get; private set; }
        [DataMember]
        public List<Prescription> prescriptions { get; private set; }
        [DataMember]
        public List<Alert> alerts { get; private set; }

        public Patient(string name, string surname, string PESEL, List<Diagnosis> diagnosis, List<Treatment> treatment, List<Test> test, List<Prescription> prescriptions, List<Alert> alerts)
        {
            this.name = name;
            this.surname = surname;
            this.PESEL = PESEL;
            this.diagnosisHistory = diagnosis;
            this.treatmentHistory = treatment;
            this.testHistory = test;
            this.prescriptions = prescriptions;
            this.alerts = alerts;
        }

    }
}
