﻿using System;
using System.ServiceModel;

namespace Interface
{
    [ServiceContract(CallbackContract = typeof(IClient), SessionMode = SessionMode.Required)]
    public interface IServer
    {
        [OperationContract(IsInitiating = true)]
        bool Authenticate();

        [OperationContract(IsInitiating = false)]
        bool Register(string userName, string password, Helpers.AccessLevel level);

        [OperationContract(IsInitiating = false)]
        Tuple<Helpers.AccessLevel, int> Login(string username, string password);

        [OperationContract]
        Patient GetPatient(string pesel);

        [OperationContract]
        Patient GetPatientPrescriptions(string pesel);

        [OperationContract]
        bool AddTest(string pesel, int doctor, string test, string result);

        [OperationContract]
        bool AddPatient(string pesel, string name, string surname, bool isMale);

        [OperationContract]
        bool AddDiagnosis(string pesel, int doctor, string result);

        [OperationContract(IsInitiating = false, IsTerminating = true, IsOneWay = true)]
        void Disconnect();
    }
}


