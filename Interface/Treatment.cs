﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    [Serializable]
    public class Treatment : MedicalRecord
    {
        public Treatment(int diagnostic, string body) : base(diagnostic, body)
        {

        }
    }
}
