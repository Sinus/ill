﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    [Serializable]
    public class Test : MedicalRecord
    {
        public string result { get; private set; }

        public Test(int diagnostic, string body, string result) : base(diagnostic, body)
        {
            this.result = result;
        }
    }
}
