﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace Interface
{
    public class Helpers
    {

        public enum AccessLevel : int { None = 0, Nurse, Doctor, Admin };
        public static string EncryptPassword(string password)
        {
            using (MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                string hash = ByteToString(md5.ComputeHash(StringToByte(password)));
                return hash;
            }
        }

        public static byte[] StringToByte(string str)
        {
            byte[] bytes = new byte[str.Length];
            int j = 0;
            foreach (byte i in System.Text.Encoding.UTF8.GetBytes(str.ToCharArray()))
            {
                bytes[j] = i;
                j++;
            }
            return bytes;

        }

        public static string ByteToString(byte[] bytes)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                sb.Append(bytes[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}
