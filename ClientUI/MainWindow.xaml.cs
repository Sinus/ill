﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Interface;

namespace ClientUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : ModernWindow
    {
        private string pesel;
        private string peselOnClick;

        private int currentId;

        public MainWindow(int id)
        {
            InitializeComponent();
            HideButtons(); //danger
            currentId = id;
        }

        private async void PatientsClick(object sender, RoutedEventArgs e)
        {
            peselOnClick = pesel;
            Patients.IsEnabled = false;
            await Client.GetPatientAsync(pesel, DisplayPatient);
        }

        private void DisplayPatient(Patient patient)
        {
            Patients.IsEnabled = true;
            if (patient == null)
            {
                HideButtons();
                AddPatient.Visibility = Visibility.Visible;
                Alerts.Text = "";
                Data.Text = "Patient with given pesel is not in our system";
            }
            else
            {
                ShowButtons();
                AddPatient.Visibility = Visibility.Hidden;
                Alerts.Text = "";
                foreach (Alert a in patient.alerts)
                    Alerts.Text += a.body + " ";

                Data.Text = "Name: " + patient.name + "\n" +
                    "Surname: " + patient.surname + "\n" +
                    "Prescriptions: ";
                ShowMedical(patient.prescriptions);
                Data.Text += "Diagnosis: ";
                ShowMedical(patient.diagnosisHistory);
                Data.Text += "Tests: ";
                ShowMedical(patient.testHistory);
                Data.Text += "Treatments: ";
                ShowMedical(patient.treatmentHistory);
            }
        }

        private void PeselChanged(object sender, TextChangedEventArgs e)
        {
            pesel = PeselBox.Text;
        }

        private void ShowMedical<T> (List<T> data) where T : MedicalRecord
        {
            foreach (T t in data)
                Data.Text += t.body + " ";
            Data.Text += System.Environment.NewLine;
        }

        private void HideButtons()
        {
            AddTest.Visibility = Visibility.Hidden;
            AddDiagnosis.Visibility = Visibility.Hidden;
            AddPatient.Visibility = Visibility.Hidden;
        }

        private void ShowButtons()
        {
            AddTest.Visibility = Visibility.Visible;
            AddDiagnosis.Visibility = Visibility.Visible;
        }

        /* Unfortunetely, this code does not compile
        private void PopAddMedicalRecord<T>() where T : new(string, int) <------
        {
            T popup = new T(pesel, currentId);
            App.Current.MainWindow = popup;
            popup.Show();
        }
        */

        private void AddTestClick(object sender, RoutedEventArgs e)
        {
            AddTestWindow addTest = new AddTestWindow(peselOnClick, currentId);
            App.Current.MainWindow = addTest;
            addTest.Show();
        }

        private void AddDiagnosisClick(object sender, RoutedEventArgs e)
        {
            AddDiagnosisWindow diagnos = new AddDiagnosisWindow(currentId, peselOnClick);
            App.Current.MainWindow = diagnos;
            diagnos.Show();
        }

        private void AddPatientClick(object sender, RoutedEventArgs e)
        {
            AddPatientWindow newPatient = new AddPatientWindow(peselOnClick, currentId);
            App.Current.MainWindow = newPatient;
            newPatient.Show();
        }
    }
}
