﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Interface;


namespace ClientUI
{
    public class ClientNurse : Interface.IClient
    {
        static ClientNurse self = new ClientNurse();
        static ServerConnection serv = new ServerConnection(self);
        static bool authed = false;

        public void BroadcastMessage(string message)
        {

        }

        public static async Task GetPatientAsync(string pesel, Action<Patient> finished)
        {
            Patient patient = self.GetPatient(pesel);
            finished(patient);
        }

        private Patient GetPatient(string pesel)
        {
            return serv.GetPatientPrescriptions(pesel);
        }
    }
}