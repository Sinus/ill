﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Interface;

namespace ClientUI
{
    /// <summary>
    /// Interaction logic for AddTestWindow.xaml
    /// </summary>
    public partial class AddTestWindow : ModernWindow
    {
        private string test, result, pesel;
        private int doctorId;

        public AddTestWindow()
        {
            InitializeComponent();
        }

        public AddTestWindow(string pesel, int doctor)
        {
            InitializeComponent();
            this.pesel = pesel;
            this.doctorId = doctor;
        }

        private async void ConfirmClicked(object sender, RoutedEventArgs e)
        {
            await Client.AddTestAsync(pesel, doctorId, test, result, ShowResult);
        }

        private void ShowResult(bool result)
        {
            var v = new ModernDialog
            {
                Title = "ILLustrator",
            };
            if (result == true)
                v.Content = "Successfully added test.";
            else
                v.Content = "Test was not added.";
            v.ShowDialog();
            this.Close();
        }

        private void TestChanged(object sender, TextChangedEventArgs e)
        {
            test = TestBox.Text;
        }

        private void ResultChanged(object sender, TextChangedEventArgs e)
        {
            result = ResultBox.Text;
        }
    }
}
