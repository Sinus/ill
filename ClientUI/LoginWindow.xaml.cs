﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Interface;

namespace ClientUI
{
    public partial class LoginWindow : ModernWindow
    {
        private string username, password;
        private static Client controller = new Client();

        public LoginWindow()
        {
            InitializeComponent();
        }

        private void LoginChanged(object sender, TextChangedEventArgs e)
        {
            username = LoginBox.Text;
        }

        private void PasswordChanged(object sender, RoutedEventArgs args)
        {
            
            password = PasswordBox.Password;
        }

        private async void LoginButtonClick(object sender, RoutedEventArgs e)
        {
            if (username == "" || password == "")
            {
                MessageBox.Show("Login and password must not be empty.");
                return;
            }

            LoginButton.IsEnabled = false;
            await Client.LoginAsync(username, password, LoginDone);

        }

        public void LoginDone(Helpers.AccessLevel loginResult, int id)
        {
            LoginButton.IsEnabled = true;

            switch (loginResult)
            {
                case Helpers.AccessLevel.Admin:
                    RegisterWindow register = new RegisterWindow();
                    App.Current.MainWindow = register;
                    this.Close();
                    register.Show();
                    break;
                case Helpers.AccessLevel.Doctor:
                    MainWindow main = new MainWindow(id);
                    App.Current.MainWindow = main;
                    this.Close();
                    main.Show();
                    break;
                case Helpers.AccessLevel.Nurse:
                    var v = new ModernDialog
                    {
                        Title = "ILLustrator",
                        Content = "Nurse (NYI)"
                    };
                    v.ShowDialog();
                    break;
                default:
                    var w = new ModernDialog
                    {
                        Title = "ILLustrator",
                        Content = "Login failed"
                    };
                    w.ShowDialog();
                    break;
            }
        }
    }
}
