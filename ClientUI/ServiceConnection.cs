﻿using System;
using System.ServiceModel;
using Interface;
using System.Collections.Generic;


namespace ClientUI
{
    public class ServerConnection : DuplexClientBase<IServer>, IServer
    {
        public ServerConnection(IClient callbackInstance)
            : base(callbackInstance)
        {
            //
        }

        public bool Authenticate()
        {
            return base.Channel.Authenticate();
        }

        public bool Register(string username, string password, Helpers.AccessLevel level)
        {
            return base.Channel.Register(username, password, level);
        }

        public Tuple<Helpers.AccessLevel, int> Login(string username, string password)
        {
            return base.Channel.Login(username, password);
        }

        public bool AddPatient(string pesel, string name, string surname, bool isMale)
        {
            return base.Channel.AddPatient(pesel, name, surname, isMale);
        }

        public Patient GetPatient(string pesel)
        {
            return base.Channel.GetPatient(pesel);
        }

        public bool AddTest(string pesel, int doctor, string test, string result)
        {
            return base.Channel.AddTest(pesel, doctor, test, result);
        }

        public bool AddDiagnosis(string pesel, int doctor, string result)
        {
            return base.Channel.AddDiagnosis(pesel, doctor, result);
        }

        public Patient GetPatientPrescriptions(string pesel)
        {
            return base.Channel.GetPatientPrescriptions(pesel);
        }

        public void Disconnect()
        {
            base.Channel.Disconnect();
        }
    }
}