﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Interface;


namespace ClientUI
{
    public class Client : Interface.IClient
    {
        static Client self = new Client();
        static ServerConnection serv = new ServerConnection(self);
        static bool authed = false;

        public void BroadcastMessage(string message)
        {

        }

        public static async Task RegisterAsync(string username, string password, Helpers.AccessLevel access, Action<bool> finished)
        {
            bool result = self.Register(username, password, access);
            finished(result);
        }

        public static async Task LoginAsync(string username, string password, Action<Helpers.AccessLevel, int> finished)
        {
            Tuple<Helpers.AccessLevel, int> state = self.Login(username, password);
            finished(state.Item1, state.Item2);
        }

        public static async Task GetPatientAsync(string pesel, Action<Patient> finished)
        {
            Patient patient = self.GetPatient(pesel);
            finished(patient);
        }

        public static async Task AddTestAsync(string pesel, int doctor, string test, string result, Action<bool> finished)
        {
            bool status = self.AddTest(pesel, doctor, test, result);
            finished(status);
        }

        public static async Task AddDiagnosisAsync(string pesel, int doctor, string result, Action<bool> finished)
        {
            bool status = self.AddDiagnosis(pesel, doctor, result);
            finished(status);
        }

        public static async Task AddPatientAsync(string pesel, string name, string surname, bool isMale, Action<bool> finished)
        {
            bool status = self.AddPatient(pesel, name, surname, isMale);
            finished(status);
        }

        private bool AddPatient(string pesel, string name, string surname, bool isMale)
        {
            return serv.AddPatient(pesel, name, surname, isMale);
        }

        private bool AddDiagnosis(string pesel, int doctor, string result)
        {
            return serv.AddDiagnosis(pesel, doctor, result);
        }

        private bool Register(string username, string password, Helpers.AccessLevel access)
        {
            password = Interface.Helpers.EncryptPassword(password);
            return serv.Register(username, password, access);
        }

        private bool AddTest(string pesel, int doctor, string test, string result)
        {
            return serv.AddTest(pesel, doctor, test, result);
        }

        private Tuple<Helpers.AccessLevel, int> Login(string username, string password)
        {
            if (authed == false)
            {
                serv.Authenticate();
                authed = true;
            }
            password = Interface.Helpers.EncryptPassword(password);
            return serv.Login(username, password);
        }

        private Patient GetPatient(string pesel)
        {
            return serv.GetPatient(pesel);
        }
    }
}
