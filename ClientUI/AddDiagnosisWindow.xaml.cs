﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientUI
{
    /// <summary>
    /// Interaction logic for AddDiagnosisWindow.xaml
    /// </summary>
    public partial class AddDiagnosisWindow : ModernWindow
    {
        private string diagnosis, pesel;
        private int doctor;

        public AddDiagnosisWindow(int id, string pesel)
        {
            InitializeComponent();
            doctor = id;
            this.pesel = pesel;
        }

        private async void ConfirmClicked(object sender, RoutedEventArgs e)
        {
            await Client.AddDiagnosisAsync(pesel, doctor, diagnosis, ShowResult);
        }

        private void ShowResult(bool result)
        {
            var v = new ModernDialog
            {
                Title = "ILLustrator",
            };
            if (result == true)
                v.Content = "Successfully added diagnosis.";
            else
                v.Content = "Diagnosis was not added.";
            v.ShowDialog();
            this.Close();
        }

        private void ResultChanged(object sender, TextChangedEventArgs e)
        {
            diagnosis = ResultBox.Text;
        }
    }
}
