﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Interface;

namespace ClientUI
{
    /// <summary>
    /// Interaction logic for RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : ModernWindow
    {

        private string username, password, passwordConfirm;
        private Helpers.AccessLevel access;

        public RegisterWindow()
        {
            InitializeComponent();
        }

        private void PasswordChanged(object sender, RoutedEventArgs e)
        {
            password = PasswordBox.Password;
        }

        private void PasswordConfirmChanged(object sender, RoutedEventArgs e)
        {
            passwordConfirm = PasswordBoxConfirm.Password;
        }

        private void LoginChanged(object sender, TextChangedEventArgs e)
        {
            username = LoginBox.Text;
        }

        private async void RegisterButtonClick(object sender, RoutedEventArgs e)
        {
            RegisterButton.IsEnabled = false;

            if (password == passwordConfirm)
                await Client.RegisterAsync(username, password, access, RegistrationDone);
            else
            {
                var v = new ModernDialog
                {
                    Title = "ILLustrator",
                    Content = "Passwords do not match."
                };
                v.ShowDialog();
                RegisterButton.IsEnabled = true;
            }
        }

        private void RegistrationDone(bool result)
        {
            if (result == true)
            {
                var v = new ModernDialog
                {
                    Title = "ILLustrator",
                    Content = "Registration successful."
                };
                v.ShowDialog();
            }
            else
            {
                var v = new ModernDialog
                {
                    Title = "ILLustrator",
                    Content = "Registration failed."
                };
                v.ShowDialog();
            }
            RegisterButton.IsEnabled = true;
        }

        private void AccesssSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var combo = sender as ComboBox;
            string val = combo.SelectedItem as string;
            access = (Helpers.AccessLevel)Enum.Parse(typeof(Helpers.AccessLevel), val);
        }

        private void AccesssLoaded(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            data.Add("Nurse");
            data.Add("Doctor");
            data.Add("Admin");

            var combo = sender as ComboBox;
            combo.ItemsSource = data;
            combo.SelectedIndex = 0;
        }
    }
}
