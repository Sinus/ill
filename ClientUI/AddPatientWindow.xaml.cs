﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientUI
{
    /// <summary>
    /// Interaction logic for AddPatientWindow.xaml
    /// </summary>
    public partial class AddPatientWindow : ModernWindow
    {
        private string pesel, name, surname;
        private bool isMale;
        private int doctor;

        public AddPatientWindow(string pesel, int doctor)
        {
            InitializeComponent();
            this.pesel = pesel;
            this.doctor = doctor;
            PeselBlock.Text = pesel;
            isMale = true;
        }

        private void NameChanged(object sender, TextChangedEventArgs e)
        {
            this.name = NameBox.Text;
        }

        private void SurnameChanged(object sender, TextChangedEventArgs e)
        {
            this.surname = SurnameBox.Text;
        }

        private void GenderSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var combo = sender as ComboBox;
            string val = combo.SelectedItem as string;

            if (val == "Male")
                isMale = true;
            else
                isMale = false;
        }

        private void GenderLoaded(object sender, RoutedEventArgs e)
        {
            List<string> data = new List<string>();
            data.Add("Male");
            data.Add("Female");

            var combo = sender as ComboBox;
            combo.ItemsSource = data;
            combo.SelectedIndex = 0;
        }

        private async void ConfirmClicked(object sender, RoutedEventArgs e)
        {
            Confirm.IsEnabled = false;
            await Client.AddPatientAsync(pesel, name, surname, isMale, ShowResult);
        }

        private void ShowResult(bool result)
        {
            var v = new ModernDialog
            {
                Title = "ILLustrator",
            };
            if (result == true)
                v.Content = "Successfully added new patient.";
            else
                v.Content = "Patient was not added.";
            v.ShowDialog();
            this.Close();
        }
    }
}
