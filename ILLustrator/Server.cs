﻿using System;
using System.ServiceModel;
using System.Data;
using System.Linq;
using System.Threading;
using System.Security.Cryptography;
using System.Data.SqlClient;
using System.Collections.Generic;
using Server;
using Interface;

namespace Server
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class Server : IServer
    {
        public event EventHandler<UserConnectedEventArgs> UserConnected;
        private SqlConnection conn;
        private Alerter alerter;
        private const string connString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=c:\users\stefan\documents\visual studio 2013\Projects\ILLustrator\ILLustrator\Database.mdf;Integrated Security=True;";

        public Server()
        {
            conn = new SqlConnection(connString);
            conn.Open();
            alerter = Alerter.GetInstance();
        }

        ~Server()
        {
            conn.Close();
        }

        public bool Register(string username, string password, Helpers.AccessLevel level)
        {
            if (checkStrings(username, password) == false)
                return false;

            username = CleanString(username);
            int accessLevel = (int)level;
            SqlTransaction transaction = conn.BeginTransaction();
            SqlCommand command = conn.CreateCommand();
            bool taken = false; // is userName taken?
            command.Connection = conn;
            command.Transaction = transaction;
            try
            {
                command.CommandText = "SET IDENTITY_INSERT Users OFF";
                command.ExecuteNonQuery();
                command.CommandText = "SELECT * FROM Users WHERE Login = '" + username + "';";
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                        taken = true;
                }
                if (taken == true)
                {
                    transaction.Commit();
                    return false;
                }
                else
                {
                    byte[] salt = MakeSalt();
                    salt = Helpers.StringToByte(Helpers.ByteToString(salt));
                    string hashed = HashPassword(password, Helpers.StringToByte(Helpers.ByteToString(salt)));

                    command.CommandText = "INSERT INTO Users (Login, Password, Salt, AccessLevel) VALUES ('" +
                        username + "', '" + hashed + "', '" + Helpers.ByteToString(salt) + "', '" + accessLevel.ToString() + "');";
                    command.ExecuteNonQuery();
                }
                transaction.Commit();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("catch: {0} {1}", e.GetType(), e.Message);
                try
                {
                    transaction.Rollback();
                    return false;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public bool AddPatient(string pesel, string name, string surname, bool isMale)
        {
            if (checkStrings(pesel, name, surname) == false)
                return false;

            name = CleanString(name);
            surname = CleanString(surname);

            SqlCommand command = conn.CreateCommand();
            command.Connection = conn;

            try
            {
                command.CommandText = "INSERT INTO Patients (\"PESEL\", \"Name\", \"Surname\", \"isMale\") VALUES (";
                command.CommandText += "'" + pesel + "', '" + name + "', '" + surname + "', '" + isMale + "'";
                command.CommandText += ");";

                int res = command.ExecuteNonQuery();

                if (res == 1)
                {
                    //updated
                    return true;
                }
                else
                {
                    //not updated
                    return false;
                }
            }
            catch (Exception e)
            {
                //not updated either
                Console.WriteLine("catch: {0} {1}", e.GetType(), e.Message);
                return false;
            }
        }

        public Tuple<Helpers.AccessLevel, int> Login(string username, string password)
        {
            if (checkStrings(username, password) == false)
                return new Tuple<Helpers.AccessLevel,int>(Helpers.AccessLevel.None, 0);

            username = CleanString(username);
            Helpers.AccessLevel accessLevel;
            int id;

            Tuple<Helpers.AccessLevel, int> result;

            bool isTaken = false;
            SqlCommand command = conn.CreateCommand();
            command.Connection = conn;
            try
            {
                command.CommandText = "SET IDENTITY_INSERT Users OFF";
                command.ExecuteNonQuery();
                command.CommandText = "SELECT * FROM Users WHERE Login = '" + username + "';";
                Console.WriteLine(command.CommandText);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        isTaken = true;
                        string salt = reader.GetString(reader.GetOrdinal("Salt"));
                        string dbHash = reader.GetString(reader.GetOrdinal("Password"));
                        id = reader.GetInt32(reader.GetOrdinal("Id"));
                        string hashedPass = HashPassword(password, Helpers.StringToByte(salt));
                        string level = (reader.GetInt32(reader.GetOrdinal("AccessLevel"))).ToString();
                        if (dbHash == hashedPass)
                            accessLevel = (Helpers.AccessLevel)Enum.Parse(typeof(Helpers.AccessLevel), level);
                        else
                            accessLevel = Helpers.AccessLevel.None;

                        result = new Tuple<Helpers.AccessLevel, int>(accessLevel, id);
                        return result;
                    }
                }
                if (isTaken == false)
                    return new Tuple<Helpers.AccessLevel, int>(Helpers.AccessLevel.None, 0);
                else
                    return new Tuple<Helpers.AccessLevel, int>(Helpers.AccessLevel.None, 0); //not possible to get here, but compiler shouts
            }
            catch (Exception e)
            {
                Console.WriteLine("catch: {0} {1}", e.GetType(), e.Message);
                return new Tuple<Helpers.AccessLevel, int>(Helpers.AccessLevel.None, 0);
            }
        }

        public bool AddDiagnosis(string pesel, int doctor, string result)
        {
            if (checkStrings(pesel, result) == false)
                return false;

            pesel = CleanString(pesel);
            result = CleanString(result);

            SqlCommand command = conn.CreateCommand();
            command.CommandText = "INSERT INTO Diagnosis (\"PatientID\", \"DoctorID\", \"Body\") VALUES (";
            command.CommandText += "'" + pesel + "', " + doctor + ", '" + result + "');";

            try
            {
                int res = command.ExecuteNonQuery();
                if (res == 1)
                {
                    //updated
                    string raiseAlert = alerter.CheckAlert(result);
                    if (raiseAlert != "") //there is something to alert
                    {
                        if (AddAlert(pesel, doctor, raiseAlert) == false)
                            Console.WriteLine("Failed to automaticly add alert when parsing {0}.", raiseAlert);
                    }
                    return true;
                }
                else
                    return false; //not updated
            }
            catch (Exception e)
            {
                //not updated either
                Console.WriteLine("catch: {0} {1}", e.GetType(), e.Message);
                return false;
            }
        }

        public bool AddAlert(string pesel, int doctor, string body)
        {
            if (checkStrings(pesel, body) == false)
                return false;

            pesel = CleanString(pesel);
            body = CleanString(body);

            SqlCommand command = conn.CreateCommand();
            command.CommandText = "INSERT INTO Alert (\"PatientID\", \"DoctorID\", \"Body\") VALUES (";
            command.CommandText += "'" + pesel + "', " + doctor + ", '" + body + "');";

            try
            {
                int res = command.ExecuteNonQuery();
                if (res == 1)
                {
                    //updated
                    return true;
                }
                else
                    return false; //not updated
            }
            catch (Exception e)
            {
                //not updated either
                Console.WriteLine("catch: {0} {1}", e.GetType(), e.Message);
                return false;
            }
        }


        public bool AddTest(string pesel, int doctor, string test, string result)
        {
            if (checkStrings(pesel, test, result) == false)
                return false;

            pesel = CleanString(pesel);
            test = CleanString(test);
            result = CleanString(result);

            SqlCommand command = conn.CreateCommand();
            command.CommandText = "INSERT INTO Test (\"PatientID\", \"DoctorID\", \"Body\", \"Result\") VALUES (";
            command.CommandText += "'" + pesel + "', " + doctor + ", '" + test + "', '" + result + "');";

            try
            {
                int res = command.ExecuteNonQuery();
                if (res == 1)
                {
                    //updated
                    return true;
                }
                else
                    return false; //not updated
            }
            catch (Exception e)
            {
                //not updated either
                Console.WriteLine("catch: {0} {1}", e.GetType(), e.Message);
                return false;
            }
        }

        public Patient GetPatient(string pesel)
        {
            if (checkStrings(pesel) == false)
                return null;

            pesel = CleanString(pesel);

            string name = "", surname = ""; //so the compiler won't complain
            bool isMale, exists = false;
            SqlCommand command = conn.CreateCommand();
            try
            {
                command.CommandText = "SELECT * FROM Patients WHERE PESEL = '" + pesel + "';";
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        exists = true;
                        name = reader.GetString(reader.GetOrdinal("Name"));
                        surname = reader.GetString(reader.GetOrdinal("Surname"));
                        isMale = reader.GetBoolean(reader.GetOrdinal("isMale"));
                    }
                }
                if (exists == false)
                    return null;
                List<Test> tests = GetTests(pesel);
                List<Treatment> treatment = GetTreatment(pesel);
                List<Diagnosis> diagnosis = GetDiagnosis(pesel);
                List<Prescription> prescriptions = GetPrescriptions(pesel);
                List<Alert> alerts = GetAlerts(pesel);

                return new Patient(name, surname, pesel, diagnosis, treatment, tests, prescriptions, alerts);
            }
            catch (Exception e)
            {
                Console.WriteLine("catch: {0} {1}", e.GetType(), e.Message);
                return null;
            }
        }

        public Patient GetPatientPrescriptions(string pesel) //used by nurse
        {
            if (checkStrings(pesel)  == false)
                return null;

            pesel = CleanString(pesel);
            //get full information and extract what we want
            Patient full = GetPatient(pesel);
            if (full == null)
            {
                return null;
            }
            List<Prescription> prescriptions = new List<Prescription>();
            Console.WriteLine(full.prescriptions.Count);
            foreach (Prescription p in full.prescriptions)
            {
                if (p.isRealised == false)
                    prescriptions.Add(p);
            }

            return new Patient(full.name, full.surname, pesel, new List<Diagnosis>(), new List<Treatment>(), new List<Test>(), prescriptions, new List<Alert>());
        }


        public bool Authenticate()
        {
            IClient connectedUser = OperationContext.Current.GetCallbackChannel<IClient>();

            if (this.UserConnected != null)
            {
                this.UserConnected(this, new UserConnectedEventArgs(connectedUser));
            }

            return true;
        }

        public void Disconnect()
        {
            Console.WriteLine("{0} - Client called 'Disconnect'", DateTime.Now);
        }

        private List<Test> GetTests(string pesel)
        {
            SqlCommand command = conn.CreateCommand();
            List<Test> result = new List<Test>();
            try
            {
                command.CommandText = "SELECT * FROM Test WHERE PatientID = " + pesel + ";";
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int diagnostic = reader.GetInt32(reader.GetOrdinal("DoctorID"));
                        string body = reader.GetString(reader.GetOrdinal("Body"));
                        string res = reader.GetString(reader.GetOrdinal("Result"));
                        result.Add(new Test(diagnostic, body, res));
                    }
                    return result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("catch: {0} {1}", e.GetType(), e.Message);
                return result;
            }
        }

        private List<Diagnosis> GetDiagnosis(string pesel)
        {
            SqlCommand command = conn.CreateCommand();
            List<Diagnosis> result = new List<Diagnosis>();
            try
            {
                command.CommandText = "SELECT * FROM Diagnosis WHERE PatientID = " + pesel + ";";
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int diagnostic = reader.GetInt32(reader.GetOrdinal("DoctorID"));
                        string body = reader.GetString(reader.GetOrdinal("Body"));
                        result.Add(new Diagnosis(diagnostic, body));
                    }
                    return result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("catch: {0} {1}", e.GetType(), e.Message);
                return result;
            }
        }

        private List<Treatment> GetTreatment(string pesel)
        {
            SqlCommand command = conn.CreateCommand();
            List<Treatment> result = new List<Treatment>();
            try
            {
                command.CommandText = "SELECT * FROM Treatment WHERE PatientID = " + pesel + ";";
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int diagnostic = reader.GetInt32(reader.GetOrdinal("DoctorID"));
                        string body = reader.GetString(reader.GetOrdinal("Body"));
                        result.Add(new Treatment(diagnostic, body));
                    }
                    return result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("catch: {0} {1}", e.GetType(), e.Message);
                return result;
            }
        }

        private List<Prescription> GetPrescriptions(string pesel)
        {
            SqlCommand command = conn.CreateCommand();
            List<Prescription> result = new List<Prescription>();
            try
            {
                command.CommandText = "SELECT * FROM Prescriptions WHERE PatientID = " + pesel + ";";
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int diagnostic = reader.GetInt32(reader.GetOrdinal("DoctorID"));
                        string body = reader.GetString(reader.GetOrdinal("Body"));
                        bool realised = reader.GetBoolean(reader.GetOrdinal("IsRealised"));
                        result.Add(new Prescription(diagnostic, body, realised));
                    }
                    return result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("catch: {0} {1}", e.GetType(), e.Message);
                return result;
            }
        }

        private List<Alert> GetAlerts(string pesel)
        {
            SqlCommand command = conn.CreateCommand();
            List<Alert> result = new List<Alert>();
            try
            {
                command.CommandText = "SELECT * FROM Alert WHERE PatientID = " + pesel + ";";
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int doctor = reader.GetInt32(reader.GetOrdinal("DoctorID"));
                        string body = reader.GetString(reader.GetOrdinal("Body"));
                        result.Add(new Alert(doctor, body));
                    }
                    return result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("catch: {0} {1}", e.GetType(), e.Message);
                return result;
            }
        }

        private bool checkStrings(params string[] list)
        {
            foreach (string x in list)
                if (String.IsNullOrEmpty(x) == true || String.IsNullOrWhiteSpace(x) == true)
                    return false;
            return true;
        }

        private byte[] MakeSalt()
        {
            RandomNumberGenerator rng = new RNGCryptoServiceProvider();
            byte[] salt = new Byte[8];
            rng.GetBytes(salt);
            return salt;
        }

        private string HashPassword(string password, byte[] salt)
        {
            string secure_password;
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            //secure_password = md5(salt + md5(password))
            secure_password = Helpers.ByteToString(md5.ComputeHash(
                Helpers.StringToByte(Helpers.ByteToString(salt) + password)));
            return secure_password;
        }

        private string CleanString(string input)
        {
            char[] danger = { '\'', '"'};
            string NewString = input.TrimEnd(danger);
            return NewString;
        }
    }
}
