﻿using System;
using Interface;

namespace Server
{
    public class UserConnectedEventArgs : EventArgs
    {
        public UserConnectedEventArgs(IClient connectedUser)
        {
            this.ConnectedUser = connectedUser;
        }

        public IClient ConnectedUser { get; set; }
    }
}
