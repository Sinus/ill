﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Alerter
    {
        private List<string> danger = new List<string>();
        private static Alerter instance;
        private static bool initiated = false;
        
        private Alerter()
        {
            danger.Add("hiv");
            danger.Add("hcv");
        }

        public static Alerter GetInstance()
        {
            if (initiated == false)
            {
                instance = new Alerter();
                initiated = true;
            }
            return instance;
        }

        public string CheckAlert(string diagnosis)
        {
            foreach (string name in danger)
            {
                if (diagnosis.ToLower().Contains(name))
                    return name; // raise alert
            }
            return ""; //no alerts
        }
    }
}
