﻿using System;
using System.ServiceModel;
using System.Collections.Generic;

namespace Server
{
    class Program
    {
        static Server serviceInstance = new Server();

        static void Main()
        {
            serviceInstance.UserConnected += new EventHandler<UserConnectedEventArgs>(serviceInstance_UserConnected);

            using (ServiceHost host = new ServiceHost(serviceInstance))
            {
                host.Open();
                Console.WriteLine("Server started.\n");
                Console.WriteLine("Press [ENTER] to quit.\n");
                Console.ReadLine();
            }
        }

        static void serviceInstance_UserConnected(object sender, UserConnectedEventArgs e)
        {

        }
    }
}