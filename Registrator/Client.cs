﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Security.Cryptography;
using Interface;


namespace Registrator
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Client" in both code and config file together.
    [Obsolete("Use ClientUI", true)]
    public class Client : Interface.IClient
    {
        static Client self = new Client();
        static ServerConnection serv = new ServerConnection(self);
        static bool authed = false;

        void Interface.IClient.BroadcastMessage(string message)
        {
            throw new NotImplementedException();
        }

        public bool Register(string name, string password, Helpers.AccessLevel level)
        {
            if (authed == false)
            {
                serv.Authenticate();
                authed = true;
            }
            password = Interface.Helpers.EncryptPassword(password);
            return serv.Register(name, password, level);
        }

    }
}
