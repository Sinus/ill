﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Interface;

namespace Registrator
{
    [Obsolete("Use ClientUI", true)]
    public partial class RegisterWindow : Window
    {
        private string username, password, confirm, chosenLevel;
        private Interface.Helpers.AccessLevel accessLevel;

        private static Client controller = new Client();

        public RegisterWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                accessLevel = (Helpers.AccessLevel)Enum.Parse(typeof(Helpers.AccessLevel), AccessLevel.Text);
                if (Enum.IsDefined(typeof(Helpers.AccessLevel), accessLevel) == false)
                {
                    MessageBox.Show(chosenLevel + " is not valid access level.");
                    return;
                }

            }
            catch (ArgumentException)
            {
                MessageBox.Show(AccessLevel.Text + "is not valid access level.");
                return;
            }
            if (string.IsNullOrEmpty(password) == true || string.IsNullOrEmpty(username) == true || string.IsNullOrEmpty(confirm) == true)
                return;

            if (password != confirm)
                MessageBox.Show("Passwords do not match.");
            else
            {
                if (controller.Register(username, password, accessLevel) == true)
                    MessageBox.Show("User created!");
                else
                    MessageBox.Show("Something went wrong.");
            }
        }

        private void LoginChanged(object sender, TextChangedEventArgs e)
        {
            username = Login.Text;
        }

        private void PasswordChanged(object sender, RoutedEventArgs args)
        {
            password = Password.Password;
        }

        private void PasswordConfirmChanged(object sender, RoutedEventArgs args)
        {
            confirm = Password_Copy.Password;
        }

        private void AccessLevelChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
