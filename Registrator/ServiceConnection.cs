﻿using System;
using System.ServiceModel;
using Interface;

namespace Registrator
{
    [Obsolete("Use ClientUI", true)]
    public class ServerConnection : DuplexClientBase<IServer>, IServer
    {
        public ServerConnection(IClient callbackInstance)
            : base(callbackInstance)
        {
            //
        }

        public bool Authenticate()
        {
            return base.Channel.Authenticate();
        }

        public Helpers.AccessLevel Login(string username, string password)
        {
            throw new NotImplementedException();
        }

        public bool Register(string userName, string password, Helpers.AccessLevel level)
        {
            return base.Channel.Register(userName, password, level);
        }
        public void Disconnect()
        {
            base.Channel.Disconnect();
        }
    }
}
