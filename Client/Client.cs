﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interface;


namespace Doctor
{
    [Obsolete("Use ClientUI", true)]
    public class Client : Interface.IClient
    {
        static Client self = new Client();
        static ServerConnection serv = new ServerConnection(self);
        static bool authed = false;

        public void BroadcastMessage(string message)
        {
            throw new NotImplementedException();
        }

        public Helpers.AccessLevel Login(string username, string password)
        {
            if (authed == false)
            {
                serv.Authenticate();
                authed = true;
            }
            password = Interface.Helpers.EncryptPassword(password);
            return serv.Login(username, password);
        }
    }
}
