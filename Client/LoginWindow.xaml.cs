﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Interface;

namespace Doctor
{
     [Obsolete("Use ClientUI", true)]
    public partial class LoginWindow : Window
    {
        private string username, password;
        private static Client controller = new Client();

        public LoginWindow()
        {
            InitializeComponent();
        }

        private void LoginChanged(object sender, TextChangedEventArgs e)
        {
            username = Login.Text;
        }

        private void PasswordChanged(object sender, RoutedEventArgs args)
        {
            password = Password.Password;
        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            if (controller.Login(username, password) > Helpers.AccessLevel.None)
                MessageBox.Show("Logged in");
            else
                MessageBox.Show("Login failed");
        }
    }
}
