﻿using System;
using System.ServiceModel;
using Interface;

namespace Doctor
{
    [Obsolete("Use ClientUI", true)]
    public class ServerConnection : DuplexClientBase<IServer>, IServer
    {
        public ServerConnection(IClient callbackInstance)
            : base(callbackInstance)
        {
            //
        }

        public bool Authenticate()
        {
            return base.Channel.Authenticate();
        }

        public bool Register(string username, string password, Helpers.AccessLevel level)
        {
            throw new NotImplementedException();
        }

        public Helpers.AccessLevel Login(string username, string password)
        {
            return base.Channel.Login(username, password);
        }

        public void Disconnect()
        {
            base.Channel.Disconnect();
        }
    }
}